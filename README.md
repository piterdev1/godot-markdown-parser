# Hello!

This repo is a basic demo project containing a Markdown -> BBCode Converter so you can use MarkDown in your Godot projects. This was fully written in GDScript and utilizes Godot's resource feature. You can freely edit the scripts and config resources to change it to your needs. The project also contains a basic Markdown Label Class which automatically converts your Markdown into BBCode. If you wish to help, feel free to create issues or pull requests!

![Screenshot of the MarkDown Preview](Screenshots/Preview.png)

## What it can do

Supported syntaxes:

- Headings (up to h3)
- **Bold text**
- _Italicised text_
- **_Bold Italicised text_**
- Strikethrough ~~texts~~ text

![Screenshot of the raw MarkDown](Screenshots/Editor.png)

## What I want to implement next

I wish to add these things:

- Some sort of markdown validation to prevent issues
- Support for bold and italicised headings
- Support for hyperlinks and images
- Trimming for spaces after headings, so they're not offset
- `code`, which should change your font to a monospaced one

![Screenshot of the MarkDown converted to BBCode](Screenshots/BBCode.png)

## What I won't implement

I may do these, but not in the near future:

- Ordered and unordered lists (you can just use them as plaintext, they don't change much)
- Syntax highlighting for code
- Horizontal rules (basically a line image)
