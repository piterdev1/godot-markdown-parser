extends Resource
class_name ReplaceEntry

enum replace_types {
    REPLACE_AND_CLOSE, # Replace tag, replace next with closing tag
    REPLACE_LINE, # Close at nearest newline character
    REPLACE_ONE, # Only Replace one tag, don't close BBCODE
}

export(replace_types) var replace_type := 0

export(String) var markdown_tag := ""

export(String) var bbcode_opening_tag := ""
export(String) var bbcode_closing_tag := ""

