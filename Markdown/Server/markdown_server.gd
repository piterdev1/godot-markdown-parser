extends Node
class_name MarkDownServer

onready var config: ReplaceConfig

var rules := []

func _init(config_resource: ReplaceConfig) -> void:
	config = config_resource
	rules = config.replace_values
	print_debug(("%s rules loaded." % rules.size()))

func parse_text(text: String) -> String:
	# Convert Markdown text into bbcode
	# TODO: Add support for hyperlinks and images.
	for rule in rules:
		if rule.replace_type == rule.replace_types.REPLACE_AND_CLOSE:
			while rule.markdown_tag in text:
				text = replace_and_close(text, rule.markdown_tag, rule.bbcode_opening_tag, rule.bbcode_closing_tag)
		
		elif rule.replace_type == rule.replace_types.REPLACE_LINE:
			while rule.markdown_tag in text:
				text = replace_line(text, rule.markdown_tag, rule.bbcode_opening_tag, rule.bbcode_closing_tag)
		
		elif rule.replace_type == rule.replace_types.REPLACE_ONE:
			while rule.markdown_tag in text:
				text = replace_first_occurence(text, rule.markdown_tag, rule.bbcode_opening_tag)
				text = replace_line(text, rule.markdown_tag, rule.bbcode_opening_tag, rule.bbcode_closing_tag)
	
	return text

func replace_first_occurence(base: String, what: String, for_what: String) -> String: 
	# Replace only first occurence with bbcode.
	# This function is also for replace one mode.
	var first_pos := base.find(what)
	if first_pos == -1:
		return base
	base.erase(first_pos, what.length())
	base = base.insert(first_pos, for_what)
	return base

func replace_and_close(base: String, what: String, for_opening: String, for_closing: String) -> String:
	# Replace opening and closing tag with bbcode.
	base = replace_first_occurence(base, what, for_opening) # Open tag
	base = replace_first_occurence(base, what, for_closing) # Close tag
	return base

func replace_line(base: String, what: String, for_opening: String, closing: String) -> String:
	# Replace opening tag with bbcode tag and close on newline.
	# Cant use previously written func because we need the char position.
	# TODO: Trim trailing space for headings.

	var first_pos := base.find(what)
	

	if first_pos == -1:
		return base
	base.erase(first_pos, what.length())
	base = base.insert(first_pos, for_opening)

	# Find newline and insert a closing tag before it.
	var newline_pos := base.find("\n", first_pos)
	base = base.insert(newline_pos, closing)

	return base
