extends Node
class_name MarkDownServerOld


var markdown_both_side_bbcode_conversions = {
    "**": "b",
    "*": "i",
    "`": "code",
    "~~": "s"
}

var _markdown_one_side_bbcode_conversions = {

}

var markdown_headers = {
    "###": "font=res://Markdown/DynamicFonts/Regular/noto_sans_h3.tres",
    "##":  "font=res://Markdown/DynamicFonts/Regular/noto_sans_h2.tres",
    "#": "font=res://Markdown/DynamicFonts/Regular/noto_sans_h1.tres",  
}


func _markdown_to_bbcode(text: String) -> String:
    for both_side_conversion in markdown_both_side_bbcode_conversions.keys():
        var replace_with_first := "[%s]" % markdown_both_side_bbcode_conversions[both_side_conversion] as String
        var replace_with_last := "[/%s]" % markdown_both_side_bbcode_conversions[both_side_conversion] as String
        while both_side_conversion in text:
            text = replace_first(text,
                                both_side_conversion,
                                replace_with_first)
            text = replace_first(text,
                                both_side_conversion,
                                replace_with_last)
    
    for md_header in markdown_headers.keys():
        text = replace_line(text, md_header, markdown_headers[md_header])
    return text


func replace_first(text: String, from: String, to: String) -> String:
    var out_text := text
    var starting_char_pos := text.find(from)

    if starting_char_pos == -1:
        return text
    out_text.erase(starting_char_pos, from.length())
    out_text = out_text.insert(starting_char_pos, to)
    return out_text

func replace_line(text: String, from: String, to_bb_name: String) -> String:
    var out_text = text

    var bbcode_open := "[%s]" % to_bb_name
    var bbcode_close := "[/%s]" % to_bb_name.split("=")[0]

    var starting_char_pos := text.find(from)

    var endl_pos := text.find("\n", starting_char_pos)
    out_text = out_text.insert(endl_pos, bbcode_close)

    
    
    if starting_char_pos == -1:
        return text
    out_text.erase(starting_char_pos, from.length())
    out_text = out_text.insert(starting_char_pos, bbcode_open)
    return out_text
    
