tool
extends RichTextLabel
class_name MarkDownLabel

export(String, MULTILINE) var markdown_test := "" setget set_markdown

var server_config := preload("res://Markdown/replace_config.tres") as ReplaceConfig
var markdown_server := MarkDownServer.new(server_config)

func _ready() -> void:
	print(server_config.replace_values.size())
	bbcode_enabled = true

func set_markdown(text: String):
	bbcode_text = markdown_server.parse_text(text)
