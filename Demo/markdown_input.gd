extends TextEdit

onready var md_label := get_node("%MarkDownLabel") as MarkDownLabel
onready var bb_label := get_node("%BBLabel") as RichTextLabel

func _ready() -> void:
    md_label.set_markdown(text)
    bb_label.set_text(md_label.bbcode_text)