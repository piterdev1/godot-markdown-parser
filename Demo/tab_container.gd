extends TabContainer

enum {
    EDITOR,
    PREVIEW,
    BBCODE,
}

onready var md_input := get_node("%TextInput") as TextEdit
onready var md_label := get_node("%MarkDownLabel") as MarkDownLabel
onready var bb_label := get_node("%BBLabel") as RichTextLabel

func _on_TabContainer_tab_changed(tab: int) -> void:
    if tab == PREVIEW:
        md_label.set_markdown(md_input.text)
    elif tab == BBCODE:
        bb_label.set_text(md_label.bbcode_text)
